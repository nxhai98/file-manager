package com.hainx.filemanager;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class ItemAdapter extends BaseAdapter {

    private List<ListItem> items;

    public ItemAdapter(List<ListItem> items) {
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder viewHolder;
        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.icon = view.findViewById(R.id.item_icon);
            viewHolder.name = view.findViewById(R.id.item_name);
            viewHolder.size = view.findViewById(R.id.item_size);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)view.getTag();
        }

        final ListItem item = items.get(position);

        viewHolder.icon.setImageResource(item.getIcon_res());
        viewHolder.size.setText(item.getSize() + " " + item.getSize_unit());
        viewHolder.name.setText(item.getName());

        return view;
    }

    static class ViewHolder {
        ImageView icon;
        TextView name;
        TextView size;
    }
}
