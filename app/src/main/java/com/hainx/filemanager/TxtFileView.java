package com.hainx.filemanager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class TxtFileView extends AppCompatActivity {
    EditText content;
    String path;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_txt_file_view);
        path = getIntent().getStringExtra("PATH");
        Log.v("TAG", path);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(path.substring(path.lastIndexOf("/")));
        actionBar.setDisplayHomeAsUpEnabled(true);

        content = findViewById(R.id.file_content);

        content.setText(readFile(path));
    }

    private String readFile(String path) {
        try {
            BufferedReader myReader = new BufferedReader(
                                            new InputStreamReader(
                                            new FileInputStream(
                                            new File(path))));
            String aBufferRow = "";
            String Buffer = "";
            while ((aBufferRow = myReader.readLine()) != null) {
                Buffer += aBufferRow + "\n";
            }
            myReader.close();

            Toast.makeText(getApplicationContext(), "Done reading " + path.substring(path.lastIndexOf("/")), Toast.LENGTH_SHORT).show();
            return Buffer;
        } catch (Exception ex) {
            Log.v("TAG", ex.getMessage());
            return "";
        }
    }

    private void writeFile(String path, EditText textView) {
        try {
            File myFile = new File(path);
            OutputStreamWriter myWriter = new OutputStreamWriter(new FileOutputStream(myFile));

            myWriter.append(textView.getText());
            myWriter.close();
            Toast.makeText(getApplicationContext(), "Writing done " + path.substring(path.lastIndexOf("/")), Toast.LENGTH_SHORT).show();
        } catch (Exception ex) {
            Log.v("TAG", ex.getMessage());
            Toast.makeText(getApplicationContext(), "Writing failed " + path.substring(path.lastIndexOf("/")), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.txt_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_save) {
            writeFile(path, content);
            return true;
        }
        return false;
    }

    @Override
    public boolean onSupportNavigateUp() {
        super.onBackPressed();
        return true;
    }
}
