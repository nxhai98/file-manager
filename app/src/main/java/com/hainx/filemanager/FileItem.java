package com.hainx.filemanager;

public class FileItem extends ListItem {
    public FileItem(String name, String uri, String parentUri, long size) {
        super(name, uri, parentUri, size);
        icon_res = R.drawable.other_file_icon;
        if (size > 1000000) {
            this.size /= 1000000;
            size_unit = "MiB";
        } else if (size > 1000) {
            this.size /= 1000;
            size_unit = "KiB";
        } else {
            if (size <= 1) {
                size_unit = "Byte";
            } else {
                size_unit = "Bytes";
            }
        }

    }
}
