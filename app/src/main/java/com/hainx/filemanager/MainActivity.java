package com.hainx.filemanager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.security.Permission;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class MainActivity extends AppCompatActivity {
    ActionBar actionBar;
    List<ListItem> items;
    private static final int STORAGE_PERMISSION_CODE = 100;
    String sdCard;
    String currentPath;
    ItemAdapter itemAdapter;
    ListView listView;
    Stack<String> listPath;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        reqPermission();
        listPath = new Stack<>();
        sdCard = Environment.getExternalStorageDirectory().getAbsolutePath();
        currentPath = sdCard;
        actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setTitle("File Manager");
        actionBar.setSubtitle(sdCard);

        actionBar.setDisplayHomeAsUpEnabled(true);

        items = new ArrayList<>();
        /* this is test data */
//        items.add(new FileItem("test1.c", "ko co", 1000));
//        items.add(new DirItem("dir1", "ko co", 1000));
//        items.add(new TxtFileItem("test1", "ko co", 1000));
        itemAdapter = new ItemAdapter(items);

        listView = findViewById(R.id.list_item);
        registerForContextMenu(listView);
        listView.setAdapter(itemAdapter);
        loadListItemOfDir(sdCard, listView);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListItem item = items.get(position);
                if (item.getIcon_res() == R.drawable.directore_icon) {
                    listPath.push(item.getParentUri());
                    currentPath = item.getUri();
                    onSelectDir(currentPath, actionBar, listView);
                } else {
                    onSelectFile(item.getUri());
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_CODE)
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED)
                Log.v("TAG", "permission denied");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.add_dir_btn) {
            // add dir
            makeDir();
            return true;
        } else if (id == R.id.add_file_btn) {
            makeFile();
            return true;
        }
        return false;
    }

    @Override
    public boolean onSupportNavigateUp() {
        try {
            currentPath = listPath.pop();
            onSelectDir(currentPath, actionBar, listView);
            return true;
        } catch (Exception ex) {
            Log.v("LOG", "exception on onSupportNavigateUp:" + ex.getMessage());
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (!listPath.isEmpty()) {
            currentPath = listPath.pop();
            onSelectDir(currentPath, actionBar, listView);
        }
        if (listPath.isEmpty()) {
            currentPath = sdCard;
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        ListView lv = (ListView) v;
        AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;

        ListItem item = (ListItem) lv.getItemAtPosition(acmi.position);

        menu.setHeaderTitle(item.getName());
        menu.add(0, acmi.position, 0,"Rename");
        menu.add(0, acmi.position, 0,"Delete");
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        if (item.getTitle().equals("Rename")) {
            showChangeNameDialog(this, item.getItemId());
        } else if (item.getTitle() == "Delete") {
            createDeleteAlertDialog(this, item.getItemId());
        } else {
            return false;
        }
        return true;
    }

    private void createDeleteAlertDialog(MainActivity mainActivity, final int pos) {
        new AlertDialog.Builder(mainActivity)
                .setTitle("Confirm")
                .setMessage("Are you sure to delete " + items.get(pos).getName())
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // delete this chose
                        String parentUri = items.get(pos).getParentUri();
                        File deleteFile = new File(items.get(pos).getUri());
                        deleteFile.delete();
                        if (deleteFile.exists()) {
                            deleteFile.getAbsoluteFile().delete();
                            if (deleteFile.exists()) {
                                getApplicationContext().deleteFile(deleteFile.getName());
                            }
                        }
                        onSelectDir(parentUri, actionBar, listView);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .create()
                .show();
    }

    private void showChangeNameDialog(final Activity context, final int pos) {
        final Dialog customDialog = new Dialog(context);
        customDialog.setTitle("Rename");
        customDialog.setContentView(R.layout.change_name_dialog);
        final EditText edt = customDialog.findViewById(R.id.new_name);
        edt.setText(items.get(pos).getName());

        ((Button) customDialog.findViewById(R.id.sd_btnOK)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt.getText().toString().trim().isEmpty()) {
                    return;
                }
                showConfirmChangeNameDialog(context, pos, edt.getText().toString());
                customDialog.dismiss();
            }
        });
        customDialog.show();
    }

    private void showConfirmChangeNameDialog(final Activity context, final int pos, final String newName) {
        new AlertDialog.Builder(context)
                .setTitle("Confirm")
                .setMessage("Are you sure to change name " + items.get(pos).getName() + " to: " + newName)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        File from = new File(items.get(pos).getUri());
                        File to = new File(items.get(pos).getParentUri(), newName);
                        if (from.renameTo(to)) {
                             Toast.makeText(context, "Renamed", Toast.LENGTH_SHORT).show();
                             onSelectDir(items.get(0).getParentUri(), actionBar, listView);
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .create()
                .show();
    }

    private void reqPermission() {
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        }
        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        }
    }

    /* loadListItemOfDir
     * chức năng: lấy ra danh sách thành viên của Directory và thêm vào listview
     * */
    private void loadListItemOfDir(String uri, ListView listView) {
        try {
            Log.v("TAG", uri);
            File directory = new File(uri);
            File[] files = directory.listFiles();
            items = new ArrayList<>();
            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    if (files[i].isDirectory()) {
                        items.add(new DirItem(files[i].getName(), files[i].getAbsolutePath(), uri, files[i].listFiles().length));
                    } else {
                        items.add(new FileItem(files[i].getName(), files[i].getAbsolutePath(), uri, files[i].length()));
                    }
                }
            }
            itemAdapter = new ItemAdapter(items);
            listView.setAdapter(itemAdapter);
        } catch (Exception ex) {
            Log.v("TAG", "Failed in loadListItemOfDir: " + ex.getMessage());
        }


    }

    /*
     * Hàm thực hiên việc chọn directory
     * Chức năng: thay đổi listview là danh sách phần tử cử directory
     * @uri: đường dẫn của directory cần mở
     * @actionBar: cần dùng để thay đổi subtitle là @uri
     * */
    private void onSelectDir(String uri, ActionBar actionBar, ListView listView) {
        actionBar.setSubtitle(uri);
        loadListItemOfDir(uri, listView);
    }

    /*
     *  Hàm thực hiện việc chọn txt file
     *  Chức năng: mở một activity mới có thể xem và sửa đổi file txt
     */
    private void onSelectFile(String uri) {
        String extension = uri.substring(uri.lastIndexOf(".") + 1);
        Log.v("TAG", extension);
        if (extension.equals("txt")) {
            Intent intent = new Intent(MainActivity.this, TxtFileView.class);
            intent.putExtra("PATH", uri);
            startActivity(intent);
            onSelectDir(currentPath, actionBar, listView);
        }

    }

    /* addFile
     * Chức năng: thêm một file mới vào thư mục hiện tại, mở actyvity mới để thêm nội dung
     * */
    private void addFile(String parentUri, String name) {
        onSelectFile(parentUri + "/" + name);
    }

    /* makeDir
     * chức năng: thêm một thư mục mới
     */
    private void  makeDir() {
        final Dialog customDialog = new Dialog(this);
        customDialog.setTitle("Name of directory");
        customDialog.setContentView(R.layout.new_dialog);
        final EditText edt = customDialog.findViewById(R.id.new_name);

        ((Button) customDialog.findViewById(R.id.sd_btnOK)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt.getText().toString().trim().isEmpty()) {
                    return;
                }
                File newDir = new File(currentPath + "/" +edt.getText());
                newDir.mkdir();
                onSelectDir(currentPath, actionBar, listView);
                customDialog.dismiss();
            }
        });
        customDialog.show();
    }

    private void makeFile() {
        final Dialog customDialog = new Dialog(this);
        customDialog.setTitle("Name of file");
        customDialog.setContentView(R.layout.new_dialog);
        final EditText edt = customDialog.findViewById(R.id.new_name);
        final TextView tv = customDialog.findViewById(R.id.dialog_title);
        tv.setText("Name for new File");
        ((Button) customDialog.findViewById(R.id.sd_btnOK)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt.getText().toString().trim().isEmpty()) {
                    return;
                }
                String path = currentPath + "/" +edt.getText() + ".txt";
                File newDir = new File(path);
                onSelectFile(path);
                customDialog.dismiss();
            }
        });
        customDialog.show();
    }

    @Override
    protected void onResume() {
        onSelectDir(currentPath, actionBar, listView);
        super.onResume();
    }
}
