package com.hainx.filemanager;

public class TxtFileItem extends FileItem {
    public TxtFileItem(String name, String uri, String parentUri, long size) {
        super(name + ".txt", uri, parentUri, size);
        icon_res = R.drawable.txt_file_icon;
        size_unit = "";
    }
}
