package com.hainx.filemanager;

public class ListItem {
    protected String name;
    protected String uri;
    protected String parentUri;
    protected long size;
    protected int icon_res;
    protected String size_unit;

    public ListItem(String name, String uri, long size) {
        this.name = name;
        this.uri = uri;
        this.size = size;
    }

    public ListItem(String name, String uri, String parentUri, long size) {
        this.name = name;
        this.uri = uri;
        this.parentUri = parentUri;
        this.size = size;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public int getIcon_res() {
        return icon_res;
    }

    public void setIcon_res(int icon_res) {
        this.icon_res = icon_res;
    }

    public String getSize_unit() {
        return size_unit;
    }

    public void setSize_unit(String size_unit) {
        this.size_unit = size_unit;
    }

    public String getParentUri() {
        return parentUri;
    }

    public void setParentUri(String parentUri) {
        this.parentUri = parentUri;
    }
}
