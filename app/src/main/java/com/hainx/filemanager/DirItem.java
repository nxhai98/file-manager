package com.hainx.filemanager;

public class DirItem extends ListItem {
    public DirItem(String name, String uri, String parentUri, long size) {
        super(name, uri, parentUri, size);
        if (size <= 1) {
            size_unit = "item";
        } else {
            size_unit = "items";
        }
        icon_res = R.drawable.directore_icon;
    }
}
